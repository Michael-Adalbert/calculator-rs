extern crate regex;

use regex::*;

pub enum Token {
    Plus,
    Minus,
    Mul,
    Div,
    Number(f64),
    CloseParentheses,
    OpenParentheses,
    Unknown,
}

pub fn split(exp: &str) -> Vec<Token> {
    let reg: Regex = regex::Regex::new(r"((([0]|[1-9][0-9]*)(\.[0-9]*)?)|(\+|-|/|\*)|(\(|\)))").unwrap();
    let mut infix: Vec<Token> = vec![];
    for cap in reg.captures_iter(exp) {
        let as_num = &cap[0].parse::<f64>();
        let token = match as_num {
            Ok(num) => Token::Number(*num),
            Err(_e) => match &cap[0] {
                "+" => Token::Plus,
                "-" => Token::Minus,
                "/" => Token::Div,
                "*" => Token::Mul,
                "(" => Token::OpenParentheses,
                ")" => Token::CloseParentheses,
                _ => Token::Unknown,
            }
        };
        infix.push(token);
    }
    infix
}