use crate::tokenizer::token::{split, Token};

fn priority(tok: &Token) -> i8 {
    match tok {
        Token::Plus | Token::Minus => 1,
        Token::Div | Token::Mul => 2,
        _ => 0,
    }
}

fn do_op(a: f64, b: f64, tok: &Token) -> f64 {
    match tok {
        Token::Plus => a + b,
        Token::Minus => a - b,
        Token::Mul => a * b,
        Token::Div => a / b,
        _ => 0.0,
    }
}

pub fn eval(str: &str) -> f64 {
    let v: Vec<Token> = split(str);
    let mut ops: Vec<Token> = vec![];
    let mut val: Vec<f64> = vec![];
    for token in v {
        match token {
            Token::OpenParentheses => ops.push(token),
            Token::Number(f) => {
                val.push(f);
            }
            Token::Plus | Token::Minus | Token::Mul | Token::Div => {
                while !ops.is_empty()
                    && priority(ops.last().expect("An operator was expected")) >= priority(&token)
                {
                    let val2 = val.pop().expect("A number was expected");
                    let val1 = val.pop().expect("A number was expected");
                    let operator = ops.pop().expect("An operator was expected");
                    val.push(do_op(val1, val2, &operator));
                }
                ops.push(token);
            }
            Token::CloseParentheses => {
                loop {
                    if ops.is_empty() {
                        break;
                    }
                    let op = ops.pop().expect("We had expected a token");
                    match op {
                        Token::OpenParentheses => {
                            break;
                        }
                        Token::Plus | Token::Mul | Token::Minus | Token::Div => {
                            let val2 = val.pop().expect("A number was expected");
                            let val1 = val.pop().expect("A number was expected");
                            val.push(do_op(val1, val2, &op));
                        }
                        _ => {}
                    };
                }
            }
            _ => {}
        };
    }
    while !ops.is_empty() {
        let val2 = val.pop().expect("A number was expected");
        let val1 = val.pop().expect("A number was expected");
        let operator = ops.pop().expect("An operator was expected");
        val.push(do_op(val1, val2, &operator));
    }
    val.pop().expect("A value was expected")
}
