pub mod evaluation;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(evaluation::eval("1+1"), 2.0);
        assert_eq!(evaluation::eval("1+1+2"), 4.0);
    }

    #[test]
    fn test_mul() {
        assert_eq!(evaluation::eval("1*1"), 1.0);
        assert_eq!(evaluation::eval("1*2*3"), 6.0);
    }

    #[test]
    fn test_minus() {
        assert_eq!(evaluation::eval("1-1"), 0.0);
        assert_eq!(evaluation::eval("1-2-3"), -4.0);
    }

    #[test]
    fn test_div() {
        assert_eq!(evaluation::eval("1/2"), 0.5);
        assert_eq!(evaluation::eval("99/9"), 11.0);
    }

    #[test]
    fn test_parentheses() {
        assert_eq!(evaluation::eval("(1+1)/2"), 1.0);
        assert_eq!(evaluation::eval("(99-89)/10"), 1.0);
    }

    #[test]
    fn test_priority() {
        assert_eq!(evaluation::eval("2*2+1"), 5.0);
        assert_eq!(evaluation::eval("2*2/8"), 0.5);
    }
}